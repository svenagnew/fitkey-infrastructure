# README #

This directory contains Terraform code for instantiating and managing the Fitkey infrastructure (excluding the Heroku platform)

## Quick Start ##

Clone the fitkey-infrastructure repo to your local system.

```
~:$ git clone git@github.com:unboxed/fitkey-infrastructure.git
```

Change into the Terraform directory and copy `terraform.tfvars.example` to `terraform.tfvars`

```
~:$ cd fitkey-infrastructure/terraform && cp terraform.tfvars.example terraform.tfvars
```

Fill in the required variable values to make them available to Terraform


### Additional Information ###

Read the official [Terraform documentation](https://terraform.io/docs/index.html)
