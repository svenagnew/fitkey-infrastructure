# Create the Digital Ocean provider
provider "digitalocean" {
  token = "${var.do_api_token}"
}

provider "aws" {
  alias = "ubxd"

  access_key = "${var.aws_key}"
  secret_key = "${var.aws_secret}"

  region = "us-east-1"
}

resource "digitalocean_droplet" "fitkey-staging-server" {
    image = "ubuntu-14-04-x64"
    name = "fitkey-staging-server"
    region = "lon1"
    size = "4gb"
    ssh_keys = [307489]
}

resource "aws_route53_record" "fitkey-staging-server" {
  provider = "aws.ubxd"
  zone_id = "Z3PI72UHF494G2"
  name = "fitkey-staging-server.ubxd.io"
  type = "A"
  ttl = "300"
  records = ["${digitalocean_droplet.fitkey-staging-server.ipv4_address}"]
}
