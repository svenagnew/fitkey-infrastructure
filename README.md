# README #

This repository contains the code required to manage the [fitkey](http://www.fitkey.co.za) infrastructure&ast; and services.

* [Ansible](http://www.ansible.com/home) - configuration management and deployment automation
* [Packer](https://packer.io) - machine image definitions for Vagrant and Digital Ocean
* [Vagrant](https://www.vagrantup.com) - portable Virtual-Machine-backed development environments
* [Serverspec](http://serverspec.org) - Test Driven infrastructure
* [Terraform](https://terraform.io) - Infrastructure as Code

Each sub-directory contains it's own **README** file with information on how to get started. Components should be used in an orchestrated manner and frequently depend on one another to produce the desired state in your platform.

## Requirements ##

This repository assumes a Mac OSx development environment with the following installed:

+ Ansible ( 1.9+ )
+ Homebrew
+ Packer
+ Rbenv ( or a Ruby Version Manager )
+ Ruby ( 1.9.3+ )
+ Terraform ( v0.5.0+)
+ Vagrant

## Quick Start ##

```bash
~:$ git clone git@github.com:unboxed/fitkey-infrastructure.git

~:$ cd fitkey-infrastructure && bundle install
```

## Getting Started Guide ##

Once you've cloned a copy of the repo, the basic workflow should roughly be as follows:

1. Define a set of tests for a host using `serverspec` - run these tests on your local machine against either a non-existant cloud host or a vagrant vm and watch them fail.

2. Using either a combination of Terraform and Ansible to provision and configure cloud-hosts or a combination of Vagrant and Ansible to provision and configure a locally running development environment, write the required code to produce a machine which satisfies the desired state declared in your Serverspec tests.

3. Once you've written the necessary provisioning and configuration code, deploy your infrastructure.

4. Now, run the Serverspec tests again and bask in the green-glow of your test-driven-infrastructure.

## NOTES ##

It is recommended to keep this infrastructure repository as a sibling of the [fitkey-eu](https://github.com/unboxed/fitkey-eu) Rails API server repository.

----  
&ast; excludes the production [Heroku](http://heroku.com) platform
