# Contributing #

The Fitkey Infrastructure is a team effort. Your contributions are welcome.

When submitting a patch, please create a Pull Request using the PR feature on [Github](https://github.com)

We don't have a defined Bug Tracking system in place yet, so for now, please use our [Trello Board](https://trello.com/b/x36HfOZR/fitkey-backlog) and create a backlog item. We use [BugSnag](https://bugsnag.com/fitkey/fitkey-server/errors) for application error reporting so please try to reference a BugnSnag issue in your Trello card...
