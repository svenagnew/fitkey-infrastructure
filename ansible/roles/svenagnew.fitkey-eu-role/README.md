fitkey-eu-role
=========

This role manages all the tasks for uniquely configuring a system to act as a deployment platform for the Fitkey-eu Rails application.

Although deployment of the Rails source code is managed at this stage by Capistrano, this Ansible role prepares the target system for Capistrano deployment.

Requirements
------------

Ensure you have provisioned the base infrastructure using Terraform.

See http://github.com/unboxed/fitkey-infrastructure#README.md for more information.

Role Variables
--------------

- `fitkey_eu_repo_path`: set this to the path on your system where the `fitkey-eu` project repo is located e.g. `/Users/sven/Developer/Ubxd/Fireworks/fitkey-eu`

Dependencies
------------

+ ANXS.postgresql
+ unboxed.common
+ unboxed.nodejs
+ unboxed.nginx-rails
+ znzj.rbenv

Example Playbook
----------------

Simply include the role on all hosts you wish to act as application servers for fitkey-eu

    - hosts: servers
      roles:
         - { role: unboxed.fitkey-eu-role, fitkey_eu_repo_path: ~/work/ }

License
-------

Copyright (c) 2015 Sven Agnew All Rights Reserved.

Author Information
------------------

- Sven Agnew
  @kangohammer
  http://ideamachine.co.za
