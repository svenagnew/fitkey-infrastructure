#!/bin/bash -eux
# Sets up a basic ruby development environment with rbenv and bundler

# Install required packages
sudo apt-get -y install curl git-core libcurl4-openssl-dev libqt4-dev libreadline-dev libsqlite3-dev libssl-dev libv8-dev libxml2-dev libxslt1-dev libyaml-dev python-software-properties zlib1g-dev

# Install rbenv
sudo git clone git://github.com/sstephenson/rbenv.git /usr/local/rbenv

echo $PATH

# Add rbenv path and eval support to .bashrc and .bash_profile
# as well as to /etc/skel/* so that new users inherit it.
echo -e 'export PATH="./bin:/usr/local/rbenv/bin:$PATH"\neval "$(rbenv init -)"' | tee -a ~/.bash_profile ~/.bashrc /etc/skel/.profile /etc/skel/.bashrc /etc/profile.d/rbenv.sh
. ~/.bash_profile

echo $PATH

# Install some plugins for rbenv
git clone git://github.com/sstephenson/ruby-build.git /usr/local/rbenv/plugins/ruby-build

cd /usr/local/rbenv/plugins/ruby-build && sudo ./install.sh

git clone git://github.com/sstephenson/rbenv-gem-rehash.git /usr/local/rbenv/plugins/rbenv-gem-rehash
git clone git://github.com/rkh/rbenv-update.git /usr/local/rbenv/plugins/rbenv-update
git clone git://github.com/dcarley/rbenv-sudo.git /usr/local/rbenv/plugins/rbenv-sudo

echo $PATH

# Install 1.9.x & 2.x.x rubies, and set the default version
sudo -H -u vagrant bash -i -c rbenv install 1.9.3-p547
#rbenv install --keep 2.0.0-p481
#rbenv install --keep 2.1.0
#rbenv install --keep 2.1.1
sudo -H -u vagrant bash -i -c rbenv install 2.1.2
#rbenv install --keep 2.1.4
sudo -H -u vagrant bash -i -c rbenv global 1.9.3-p547
sudo -H -u vagrant bash -i -c ruby -v
echo -e "install: --no-ri --no-rdoc\nupdate: --no-ri --no-rdoc" > ~/.gemrc

# Install bundler
gem install bundler
